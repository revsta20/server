/**
 *   Copyright 2010 Guardian News And Media 
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 **/

package usingjsoup;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GuardianNews{
    private final String GUARDIAN_API_KEY = "aeb24e25-6c01-47e6-bd03-84dce8f980d2";
    private final String USER_AGENT = "Mozilla/5.0";
    private final String CONTENT_TYPE="application/json";
    
    public GuardianNews() throws Exception{
    sendGet();
    //with get method call json methods heret to filter through, need to look at json
    //check the gmail sent to view guardians test api and limited requests 
    }
   
    private void sendGet() throws Exception {

    
                String url = "https://content/guardianapis.com/search?api=key"+GUARDIAN_API_KEY;
                //http://content.guardianapis/com/tags?q=apple&section=technology&show-references=all
                //searches apple in their technology section? should search current news top 10 return json.
                //take json and format into a string that can be pinged to the databse.
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("Content-Type", CONTENT_TYPE);
                //con.setRequestProperty("X-Access-Token", "a641b4256339424a");
                
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
    }
}
/*
import com.gu.openplatform.contentapi.*;
import org.junit.After;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class GuardianNews {

    private static final Logger logger = LoggerFactory.getLogger(ExampleContentSearchTest.class);

    private ContentSearchResult results;

    @After
    public void checkResultsAreOk() throws Exception {
        assertTrue(results.isResponseOk());
    }

    @Test
    public void canSetPageSize() throws Exception {
        results = GuardianContentApi.contentSearchQuery().pageSize(35).execute();

        checkResponseOK();
        assertThat(results.getContent().size(), equalTo(35));
    }

    @Test
    public void testQueryParameterIsPassedThroughToEndpoint() throws Exception {
        results = GuardianContentApi.contentSearchQuery().query("azxcdfedhjslskeifm3472kvksuflasuiflow92lapqwop").execute();

        checkResponseOK();

        assertThat(results.getTotalNumberOfResults(), equalTo(0));
    }

    @Test
    public void canExecuteZeroTermQueryWithCorrectDefaultsSet() throws Exception {
        results = GuardianContentApi.contentSearchQuery().zeroTermQuery().execute();
        ContentSearchResult result2 = GuardianContentApi.contentSearchQuery().execute();

        checkResponseOK();
        assertThat(results.getStatus(), equalTo(ResponseStatus.ok));
        assertThat(results.getUserTier(), equalTo(UserTier.free));
        assertThat(results.getTotalNumberOfResults(), greaterThan(0));
        assertThat(results.getStartIndex(), equalTo(1));
        assertThat(results.getPageSize(), equalTo(10));
        assertThat(results.getCurrentPage(), equalTo(1));
        assertThat(results.getNumberOfPages(), greaterThan(0));
        assertThat(results.getOrderBy(), equalTo(OrderBy.newest));
        assertThat(results.getTotalNumberOfResults(), equalTo(result2.getTotalNumberOfResults()));
        assertThat(results.getContent().size(), equalTo(10));

        for (Content content : results.getContent())
            ContentValidator.validateContent(content);
    }

    @Test
    public void testCanRetriveMultipleRefinements() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery()
                .showRefinementsOfType("keyword", "blog")
                .execute();

        checkResponseOK();
        assertTrue(results.hasRefinements(TagType.keyword));
        assertTrue(results.hasRefinements(TagType.blog));

        for (Refinement refinement : results.getAllRefinements())
            RefinementValidator.validateRefinement(refinement);
    }

    @Test
    public void testCanReturnRefinementsOfSingleType() throws Exception {
        results = GuardianContentApi
                .contentSearchQueryWithRefinements()
                .showRefinementsOfType(TagType.keyword)
                .execute();

        checkResponseOK();
        assertTrue(results.hasRefinements());
        assertFalse(results.hasRefinements(TagType.contentType));

        for (Refinement refinement : results.getRefinements(TagType.keyword))
            RefinementValidator.validateRefinement(refinement);
    }

    @Test
    public void testCanRetrieveRefinements() throws Exception {
        results = GuardianContentApi.contentSearchQueryWithRefinements().execute();

        checkResponseOK();
        assertTrue(results.hasRefinements());

        for (Refinement refinement : results.getAllRefinements())
            RefinementValidator.validateRefinement(refinement);
    }

    @Test
    public void testCanPaginateThroughResults() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery().execute();

        checkResponseOK();
        assertThat(results.getNumberOfPages(), greaterThan(1));

        int foundPages = 1;
        for (ContentSearchQuery nextPageQuery : results.getResultPageQueries())
            foundPages = nextPageQuery.getStartPage();

        assertThat(results.getNumberOfPages(), equalTo(foundPages));
    }

    @Test
    public void testBasicPaginationWorks() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery().startPage(2).execute();

        assertThat(results.getCurrentPage(), equalTo(2));
    }

    @Test
    public void testAutomaticResultPaginationWorks() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery().execute();
        checkResponseOK();
        assertThat(results.getNumberOfPages(), greaterThan(1));

        Iterable<ContentSearchQuery> iterable = results.getResultPageQueries();
        ContentSearchQuery nextPageQuery = iterable.iterator().next();
        ContentSearchResult nextPageResult = nextPageQuery.execute();

        assertThat(nextPageResult.getCurrentPage(), equalTo(results.getCurrentPage() + 1));
    }

    @Test
    public void testCanSearchBySection() throws Exception {
        results = GuardianContentApi.contentSearchQuery()
                .section("travel")
                .execute();

        checkResponseOK();
        assertTrue(results.hasContent());

        for (Content content : results.getContent())
            assertThat(content.getSectionId(), equalTo("travel"));
    }

    @Test
    public void testMinimalClientDoesNotReturnTagsOrFields() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery()
                .zeroTermQuery()
                .execute();

        checkResponseOK();
        assertTrue(results.hasContent());

        for (Content content : results.getContent()) {
            assertTrue(content.getTags().size() == 0);
            assertTrue(content.getFieldNames().size() == 0);
        }
    }

    @Test
    public void testCanUpgradeFunctionalityOfMinimalClient() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery().showAllTagTypes().execute();
        assertTrue(results.hasContent());

        for (Content content : results.getContent())
            assertTrue(content.hasTags());
    }

    @Test
    public void testCanFilterBySpecificTag() throws Exception {
        results = GuardianContentApi.contentSearchQuery()
                .filterByTag("politics/politics")
                .showTagTypes(TagType.keyword)
                .execute();

        checkResponseOK();
        assertTrue(results.hasContent());

        for (Content content : results.getContent()) {
            assertTrue(content.hasTag("politics/politics"));
            assertTrue(content.hasTag(TagType.keyword, "politics/politics"));
            assertTrue(content.hasTag("keyword", "politics/politics"));
        }

        results = GuardianContentApi.contentSearchQuery()
                .execute();

        checkResponseOK();
        Content first = results.getFirst();
        Tag firstKeyword = first.getTagsOfType(TagType.keyword).iterator().next();
        assertThat(firstKeyword, notNullValue());

        results = GuardianContentApi.contentSearchQuery()
                .filterByTag(firstKeyword)
                .execute();

        checkResponseOK();
        assertTrue(results.hasContent());

        for (Content content : results.getContent())
            assertTrue(content.getTagsOfType(TagType.keyword).contains(firstKeyword));
    }

    @Test
    public void testCanRequestSpecificTagTypes() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery()
                .filterByTag("politics/politics")
                .section("politics")
                .showTagTypes(TagType.keyword).execute();

        checkResponseOK();
        assertTrue(results.hasContent());

        for (Content content : results.getContent()) {
            assertTrue(content.hasTagsOfType(TagType.keyword));
            assertFalse(content.hasTagsOfType("series"));
        }

        results = GuardianContentApi.minimalContentSearchQuery()
                .showTagTypes(TagType.keyword, TagType.contentType)
                .section("politics")
                .filterByTag("politics/politics")
                .execute();

        checkResponseOK();
        for (Content content : results.getContent())
            assertTrue(content.hasTagsOfType("keyword", "type"));
    }

    @Test
    public void testCanSearchForShortUrls() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery()
                .showField(FieldType.shortUrl)
                .execute();

        System.out.println(results.getContent());

        checkResponseOK();

        for (Content content : results.getContent())
            assertTrue(content.hasField(FieldType.shortUrl));
    }

    @Test
    public void testCanFilterFields() throws Exception {
        results = GuardianContentApi.minimalContentSearchQuery()
                .showField("headline")
                .execute();

        checkResponseOK();
        assertTrue(results.hasContent());

        for (Content content : results.getContent()) {
            assertTrue(content.hasField("headline"));
            assertThat(content.getField("headline"), notNullValue());
        }
    }

    @Test
    public void testCanSearchWithInterestingCharactersInQueryStrings() throws Exception {
        results = GuardianContentApi.contentSearchQuery()
                .query("This & that <or> $this ? ~that [or] (this) {and} -that/")
                .execute();

        checkResponseOK();
    }

    private void checkResponseOK() {
        if (!results.isResponseOk())
            logger.error("Invalid response recieved from API");

        assertTrue(results.isResponseOk());
    }
}



*/