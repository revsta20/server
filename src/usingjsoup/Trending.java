/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usingjsoup;
   //puppy filter:native_video   //puppy video, amplify, vine or periscope
   //puppy filter:image          //puppy images	
   //puppy filter:twimg          //atleast one or more puppy photos - really shit
   //traffic ?                   //contains traffic and is a question
/**
 *
 * @author Dylan
 */
  

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.validator.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Trends;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class Trending {
    //consumer key, secret key
    //private static final String CONTENT_TYPE="application/json";
    private static final String USER_AGENT_REDDIT = "mac:redditapp:v1 (by /u/SirDillbert)";
    static List<String> redditPosts = new ArrayList();
    static List<String> tweets = new ArrayList();
    ConfigurationBuilder cb = new ConfigurationBuilder();
    final AccessToken accessToken = new AccessToken("2941728028-tRcPUR64u4NnRtEkWx16uIKdOJlU8mcuqnhLWQk", "sApUM9gLIuOd49EfX89IILuJ3SHG4fkMJlRYZP6caRixS");
    //  final Twitter twitter = new TwitterFactory().getInstance(); 
    private static final String SUBJECT = "trending";
    List<String> tweetsToAdd;
    
    public Trending(List<String> tweetsToAdd){
        this.tweetsToAdd = tweetsToAdd;
    }
    
public List<String> gettingTwitterWithApi(){
    cb.setDebugEnabled(true)
    .setOAuthConsumerKey("DF1v9FGE8M57y3Cn6HjV9SsVx")
    .setOAuthConsumerSecret("lxCTBlw04Iem0boHJ49StyGVi2l9U44c25y2lAGXoUTVYLq92c")
    .setOAuthAccessToken("2941728028-tRcPUR64u4NnRtEkWx16uIKdOJlU8mcuqnhLWQk")
    .setOAuthAccessTokenSecret("sApUM9gLIuOd49EfX89IILuJ3SHG4fkMJlRYZP6caRixS");
    Twitter twitter = new TwitterFactory(cb.build()).getInstance(); 
    try {
     
        Query query = new Query(SUBJECT+" filter:native_video");
        QueryResult result;
     //   Trends topTrends = getPlaceTrends(1);
       
        result = twitter.search(query);
        List<Status> tweets = result.getTweets();
        for (Status tweet : tweets) {
            //System.out.println("@" + tweet.getUser().getScreenName() + " - " + tweet.getText());
            String currTweetText = tweet.getText();
            currTweetText = currTweetText.substring(currTweetText.lastIndexOf(' ') + 1);

            tweetsToAdd.add(currTweetText);
        }
        //loop again for second query with same subject matter
        query = new Query(SUBJECT+" filter:media");
        result = twitter.search(query);
        tweets = result.getTweets();
        for (Status tweet : tweets) {
            String currTweetText = tweet.getText();
            currTweetText = currTweetText.substring(currTweetText.lastIndexOf(' ') + 1);            
            tweetsToAdd.add(currTweetText);
        }
       
    }
    catch (TwitterException te) {
        te.printStackTrace();
        System.out.println("Failed to search tweets: " + te.getMessage());
        System.exit(-1);
    }
    
     tweetsToAdd = urlValidation(tweetsToAdd);
     tweetsToAdd = cleanUpLinks(tweetsToAdd);
     return tweetsToAdd;
}
 public boolean stopDuplicates(String currLink, List<String> linksSoFar){
        boolean tempVal = false;
        if(!linksSoFar.isEmpty()){
        for(int i=0; i<linksSoFar.size(); i++){
            if(linksSoFar.get(i).equals(currLink)){
                tempVal= true;
            }
          }
        }
    
 return tempVal;
}

 public List<String> cleanUpLinks(List<String> currLinks){
     if(!(currLinks.isEmpty())){
         for(int i=0; i<currLinks.size(); i++){
             String temp = currLinks.get(i);
             if(temp.contains("…") || temp.contains("...")){
                 currLinks.remove(i);
             }   
            /* if(!temp.contains("https://t.co/")){
                 currLinks.remove(i);
             }
             if(!temp.contains("http://t.co/")){
                 currLinks.remove(i);
             }*/
         }
     }
     return currLinks;
 }
 public List<String> urlValidation(List<String> linksSoFar){
     String[] schemes = {"http","https"}; // DEFAULT schemes = "http", "https", "ftp"
     UrlValidator urlValidator = new UrlValidator(schemes);
    for(int i=0;i<linksSoFar.size();i++){
        String toCompare = linksSoFar.get(i).replaceAll("[^\\x00-\\x7F]", "");
        toCompare.replace("...", "");
        toCompare.replace("…","");
    if (urlValidator.isValid(toCompare) || toCompare.contains("#")) {
       System.out.println("url is valid");
    } else {
       System.out.println("url is invalid");
       System.out.println("Link Deleted:"+toCompare);
       linksSoFar.remove(i);
    }
    }
    return linksSoFar;
 }
 
 
 public void getReddit(String topic){
    
  //  for(int j=0; j < 101; j+=25){
    try{
        String subreddit = ("http://reddit.com/&after="+USER_AGENT_REDDIT);
        System.out.println("SCANNING LINK: "+subreddit);
        //gets http protocol
        Document doc = Jsoup.connect(subreddit).userAgent(USER_AGENT_REDDIT).timeout(0).get();
        //Document doc = Jsoup.connect(subreddit).timeout(0).get();

        //get page title
        String title = doc.title();
        System.out.println("title : " + title);

        //get all links
        Elements links = doc.select("a[href]");
        
        for(Element link : links){
            //get value from href attribute
        //  String checkLink = link.attr("href");
          String checkLink = link.attr("data-href-url");
          //http://imgur
          if(checkLink.isEmpty() || checkLink.contains("https://i.reddituploads.com")){
          }else{
          if(stopDuplicates(checkLink,redditPosts) == false){
          redditPosts.add(checkLink);
          }
         }
        }
        for(int i=0; i<redditPosts.size(); i++){
            System.out.println(redditPosts.get(i));
        }
    }
    catch (IOException e){
        e.printStackTrace();
    }   
 //  }
    
   }
}